import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class SubjectInputModel {
  @Field(() => String)
  name: string;
}
