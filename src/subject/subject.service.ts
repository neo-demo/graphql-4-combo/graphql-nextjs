import { Injectable } from '@nestjs/common';
import { SubjectModel } from './subject.model';
import { PubSub } from 'apollo-server-express';

@Injectable()
export class SubjectService {
  private subjects: SubjectModel[] = [
    { id: 1, name: 'MATHS' },
    { id: 2, name: 'ECO' },
    { id: 3, name: 'PHYSICS' },
  ];

  public pubSub = new PubSub();
  topic = 'subjectAdded';

  getAllSubjects(): SubjectModel[] {
    return this.subjects;
  }

  addSubject({ name }): SubjectModel[] {
    const subject = { id: this.subjects.length + 1, name: name };
    this.subjects.push(subject);
    this.pubSub.publish(this.topic, {
      subjectAdded: { mutation: 'created', node: subject },
    });
    return this.subjects;
  }

  subScribeSubjectObservable() {
    return this.pubSub.asyncIterator(this.topic);
  }
}
