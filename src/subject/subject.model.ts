import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SubjectModel {
  @Field(() => ID)
  id: number;

  @Field(() => String)
  name!: string;
}
