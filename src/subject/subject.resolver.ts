import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { SubjectService } from './subject.service';
import { SubjectModel } from './subject.model';
import { SubjectInputModel } from './subject.input.model';

@Resolver(() => SubjectModel)
export class SubjectResolver {
  constructor(private subjectService: SubjectService) {}

  @Query((returns) => [SubjectModel]!)
  public returnAllSubject() {
    return this.subjectService.getAllSubjects();
  }

  @Mutation(() => [SubjectModel]!)
  public addSubject(@Args('subject') subject: SubjectInputModel) {
    return this.subjectService.addSubject(subject);
  }

  @Subscription(() => SubjectModel)
  public publishEvent() {
    return this.subjectService.subScribeSubjectObservable().next();
  }
}
