import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { SubjectResolver } from './subject/subject.resolver';
import { SubjectService } from './subject/subject.service';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      debug: true,
      playground: true,
      installSubscriptionHandlers: true,
    }),
  ],
  providers: [SubjectResolver, SubjectService],
})
export class AppModule {}
